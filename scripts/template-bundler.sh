#!/bin/sh

# 2 ways of using it
#
## Create <something>.tar.gz and <something>.zip files, with vars replaces,
## not touching original directory.
#
# "scripts/template-bundler.sh templates/tpl01 packages/something"
# will create packages/something.tar.gz and packages/something.zip
#
## Or change the original directory
# scripts/template-bundler.sh

set -e

TEMPLATE_DIR="$1"
OUTPUT_FILE="$2"
MODE=1

BASE_DIR="$(dirname "$0")"

if test -z "$TEMPLATE_DIR"; then
    echo "First argument is a template directory"
    exit 1
fi

if ! test -d "$TEMPLATE_DIR"; then
    echo "cannot find template directory at [$TEMPLATE_DIR]"
    exit 1
fi

if test -z "$OUTPUT_FILE"; then
    MODE=2
fi

##########

if [ $MODE -eq 1 ]; then
    # Create a temporary directory and store its name in a variable.
    TEMP_DIR=$(mktemp -d)

    # Exit if the temp directory wasn't created successfully.
    if [ ! -e "$TEMP_DIR" ]; then
        >&2 echo "Failed to create temp directory"
        exit 1
    fi

    # Make sure the temp directory gets removed on script exit.
    trap "exit 1"           HUP INT PIPE QUIT TERM
    trap 'rm -rf "$TEMP_DIR"'  EXIT

    cp -R "$TEMPLATE_DIR/." "$TEMP_DIR"

    TARGET_DIR="$TEMP_DIR"
else
    TARGET_DIR="$TEMPLATE_DIR"
fi

replace() {
    local search="$1"
    local replace="$2"
    grep -rl "$search" "$TARGET_DIR" | xargs sed -i "s|${search}|${replace}|g" 2>/dev/null \
        || { echo "No files found in $TARGET_DIR for pattern $1"; true; }
}

replace "%CI_PROJECT_URL%" "$CI_PROJECT_URL"
replace "%CI_REGISTRY_IMAGE%" "$CI_REGISTRY_IMAGE"
replace "%GO_TAG%" "$GO_TAG"
replace "%PROJECT_TAG%" "$PROJECT_TAG"
replace "%SPECIFIC_IMAGE%" "$SPECIFIC_IMAGE"
replace "%TEMPLATE_VARIATION%" "$TEMPLATE_VARIATION"

if [ $MODE -eq 1 ]; then
    tar -C "$TEMP_DIR" -zcf "${OUTPUT_FILE}.tar.gz" . 1>/dev/null
    sh -c "cd $TEMP_DIR && zip -r $(pwd)/${OUTPUT_FILE}.zip . 1>/dev/null"
    echo "Created packages [${OUTPUT_FILE}.tar.gz] and [${OUTPUT_FILE}.zip]"
else
    echo "Changes applied to [$TARGET_DIR]"
fi

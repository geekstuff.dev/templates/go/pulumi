#!make

.PHONY: go.mod-tidy
go.mod-tidy:
	go mod tidy

.PHONY: go.run
go.run:
	go run ./...

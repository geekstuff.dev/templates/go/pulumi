#!make

.PHONY: info
info:
	@echo "Project ${PROJECT_NAME}"
	@echo ""
	@echo "Commands to try:"
	@echo "  make pulumi-preview                            # Builds and runs pulumi-go app"
	@echo "  make pulumi-up                                 # Builds and applies pulumi-go app"
	@echo "  make go.mod-tidy                               # Go mod tidy"
	@echo "  make docker.distroless docker.distroless.run   # Builds and runs in container v1"
	@echo "  make docker.scratch docker.scratch.run         # Builds and runs in container v2"
	@echo "  make template.diff                             # See difference with source template"
	@echo ""
	@echo "Devcontainer features:"
	@echo "  - Use TAB to auto-complete or discover make commands"
	@echo "  - Within a devcontainer, you can use VSCode to browse other files:"
	@echo "    'code ${TPL_LIB}'"

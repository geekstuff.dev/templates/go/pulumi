#!make

# This file holds targets to help with template upgrades.

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

.PHONY: template.diff
template.diff:
	@$(MAKE) template.diff.main

.PHONY: .tmpl.diff.base
.tmpl.diff.base:
	@test -n "${SOURCE}" && test -d ${SOURCE} || { echo "SOURCE must be set and be a directory"; exit 1; }
	@diff --normal --color --recursive \
		--exclude=.local.env --exclude=ref --exclude=.git --exclude=project.env \
		--exclude=.terraform --exclude=.terraform.lock.hcl --exclude=Pulumi.*.yaml \
		--exclude=Pulumi.yaml --exclude=go.* --exclude=*.go --exclude=binary \
		${SOURCE}/ ${TPL_TEMPLATES}/${TARGET} \
		&& echo "Up to date with template" || true

.PHONY: template.diff.main
template.diff.main: SOURCE=.
template.diff.main: TARGET=
template.diff.main: .tmpl.diff.base

.PHONY: template.tag-upgrade
template.tag-upgrade:
	@test -n "${NEXT_TAG}" || { echo "NEXT_TAG must bet set"; exit 1; }
	@TARGET=.devcontainer/Dockerfile $(MAKE) .template.tag-upgrade
	@TARGET=tf-test/.gitlab-ci.yml $(MAKE) .template.tag-upgrade
	@TARGET=tf-test-vault/.gitlab-ci.yml $(MAKE) .template.tag-upgrade

.PHONY: .template.tag-upgrade
.template.tag-upgrade:
	@test -n "${NEXT_TAG}" || { echo "NEXT_TAG must be set"; exit 1; }
	@test -e "${TARGET}" \
		&& sed -i -e "s|:${DC_TAG}|:${NEXT_TAG}|g" -e "s|/${DC_TAG}|/${NEXT_TAG}|g" ${TARGET} \
		|| echo "Could not find target ${TARGET} . Skipping."

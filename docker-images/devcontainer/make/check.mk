#!make

.PHONY: check.vars
check.vars:
	@echo "Checking if the necessary vars are set.."
	@test -s ~/.dc_status \
		&& echo "Issues found:" \
		&& echo "" \
		&& cat ~/.dc_status \
		&& echo "" \
		&& echo "NOTE: In some scenario, when testing fixes or vars that involves direnv," \
		&& echo "      remember to hit enter on an empty line in your terminal" \
		&& echo "      so direnv can reload if it needs to." \
		&& exit 1 \
		|| \
		echo "No issues found!"

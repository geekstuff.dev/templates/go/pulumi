#make

VAULT_AUTH_METHOD ?= oidc
VAULT_AUTH_PATH ?= oidc

.PHONY: vault-login
vault-login: VAULT_NAMESPACE=
vault-login: .vault-login

.PHONY: .vault-login
.vault-login:
	@echo "[] Login to Vault with auth method ${VAULT_AUTH_METHOD} in NS ${VAULT_NAMESPACE} and role ${VAULT_AUTH_ROLE}"
	VAULT_NAMESPACE=${VAULT_NAMESPACE} vault login -method=${VAULT_AUTH_METHOD} -path=${VAULT_AUTH_PATH} $(shell if test -n "${VAULT_AUTH_ROLE}"; then echo "role=${VAULT_AUTH_ROLE}"; fi)
	@cp /home/dev/.vault-token ${DEVCONTAINER_VARS}/vault-token

.PHONY: vault-check
vault-check:
	@vault token lookup 1>/dev/null 2>/dev/null || { echo "Not logged into Vault"; false; }

.PHONY: vault-list-policies
vault-list-policies:
	@echo "  policies: [$(shell vault token lookup | sed -nr 's/^policies\s+\[(.*)\]/\1/p')]"
	@echo "  identity policies: [$(shell vault token lookup | sed -nr 's/^identity_policies\s+\[(.*)\]/\1/p')]"

.PHONY: vault-token-lookup
vault-token-lookup:
	vault token lookup

.PHONY: vault-token-revoke
vault-token-revoke:
	vault token revoke -self

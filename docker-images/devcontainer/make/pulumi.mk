#!make

.PHONY: pulumi-login
pulumi-login:
	pulumi login --non-interactive

.PHONY: pulumi-new-project
pulumi-new-project:
	@VAULT_TOKEN=$(shell vault print token) \
		pulumi -C ${PULUMI_PATH} new \
		$(shell if test "${FORCE}" = "1"; then echo "--force"; fi) \
		--name ${PULUMI_PROJECT} \
		--stack ${PULUMI_STACK} \
		--description "A pulumi go app" \
		--secrets-provider="${PULUMI_ENCRYPTION_PROVIDER}" \
		go
	@sed -i 's/runtime: go/runtime:\n  name: go\n  options:\n    binary: pulumi-launch/' ${PULUMI_PATH}/Pulumi.yaml

.PHONY: pulumi-stack-select
pulumi-stack-select:
	@pulumi -C ${PULUMI_PATH} stack select ${PULUMI_STACK} \
		|| { echo "Stack [${PULUMI_STACK}] cannot be selected"; false; }

.PHONY: pulumi-preview
pulumi-preview:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} preview

.PHONY: pulumi-up
pulumi-up:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} up

.PHONY: pulumi-destroy
pulumi-destroy:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} destroy

.PHONY: pulumi-stack-output
pulumi-stack-output:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} stack --non-interactive output

.PHONY: pulumi-refresh
pulumi-refresh:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} refresh $(shell if test "${FORCE}" = "1"; then echo "--force"; fi)

.PHONY: pulumi-stack-ls
pulumi-stack-ls:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} stack ls

.PHONY: pulumi-stack-info
pulumi-stack-info:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} stack --non-interactive

.PHONY: pulumi-stack-rm
pulumi-stack-rm:
	@VAULT_TOKEN=$(shell vault print token) pulumi -C ${PULUMI_PATH} stack rm

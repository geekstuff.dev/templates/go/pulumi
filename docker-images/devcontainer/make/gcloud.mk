#!make

.PHONY: gcp-full-login
gcp-full-login: gcp-login gcp-auth-default-login

.PHONY: gcp-login
gcp-login:
	@echo "[] Google login"
	gcloud auth login

.PHONY: gcp-auth-default-login
gcp-auth-default-login:
	gcloud auth application-default login

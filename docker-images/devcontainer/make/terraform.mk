#!make

.PHONY: tf-init
tf-init:
	terraform init --upgrade -input=false

.PHONY: tf-validate
tf-validate:
	terraform validate

.PHONY: tf-plan
tf-plan:
	@VAULT_TOKEN=$(shell vault print token) terraform plan

.PHONY: tf-apply
tf-apply:
	@VAULT_TOKEN=$(shell vault print token) terraform apply

.PHONY: tf-destroy-super-dangerous
tf-destroy-super-dangerous:
	@VAULT_TOKEN=$(shell vault print token) terraform destroy

.PHONY: tf-output
tf-output: tf-init
	terraform output

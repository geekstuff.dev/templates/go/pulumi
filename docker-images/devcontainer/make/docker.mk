#!make

DOCKER_BUILD_SECRETS = /tmp/secrets.${PROJECT_NAME}

.PHONY: docker.distroless docker.distroless.run
docker.distroless: DOCKER_VARIANT=distroless
docker.distroless: .docker.build
docker.distroless.run: DOCKER_VARIANT=distroless
docker.distroless.run: .docker.run

.PHONY: docker.scratch docker.scratch.run
docker.scratch: DOCKER_VARIANT=scratch
docker.scratch: .docker.build
docker.scratch.run: DOCKER_VARIANT=scratch
docker.scratch.run: .docker.run

# The specific docker build --secret used here is compatible with auto-devops
.PHONY: .docker.build
.docker.build:
	@rm -f ${DOCKER_BUILD_SECRETS}
	@echo "ABC=123" > ${DOCKER_BUILD_SECRETS}
	docker build \
		-f .docker/${DOCKER_VARIANT}.Dockerfile \
		-t ${PROJECT_NAME}/${DOCKER_VARIANT}:${DOCKER_TAG} \
		--secret id=auto-devops-build-secrets,src=${DOCKER_BUILD_SECRETS} \
		.
	@echo "docker image built: ${PROJECT_NAME}/${DOCKER_VARIANT}:${DOCKER_TAG}"
	@$(MAKE) .docker.${DOCKER_VARIANT}.image

.PHONY: .docker.run
.docker.run:
	docker run --rm -it ${PROJECT_NAME}/${DOCKER_VARIANT}:${DOCKER_TAG}

.PHONY: .docker.distroless.image .docker.scratch.image
.docker.distroless.image: DOCKER_VARIANT=distroless
.docker.distroless.image: .docker.image
.docker.scratch.image: DOCKER_VARIANT=scratch
.docker.scratch.image: .docker.image

.PHONY: .docker.image
.docker.image:
	@docker images ${PROJECT_NAME}/${DOCKER_VARIANT}:${DOCKER_TAG}

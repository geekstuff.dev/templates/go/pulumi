#!/bin/bash

set -e

if test -z "$PULUMI_PATH"; then
    echo "PULUMI_PATH is missing"
    exit 1
fi

cd $PULUMI_PATH
go build -o binary -gcflags "all=-N -l" .

if test -n "$GO_DEBUG"; then
    dlv --listen=:2345 --headless=true --api-version=2 --accept-multiclient exec ./binary
else
    ./binary
fi

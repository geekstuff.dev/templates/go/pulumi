# Pulumi Go project template

This repo builds a Pulumi Go template that includes:
- A devcontainer
  - Build from a debian image
  - With Go, Go tools, Pulumi
  - With vault, gcloud-sdk, direnv, terraform (to configure cloud buckets and Vault for Pulumi)
  - Various direnv and Makefile "libs" included (to use Pulumi, make docker images, go builds, vault)
  - Pre-built and pre-packaged (larger download, much quicker devcontainer build time)

GitLab-CI pipelines are not yet part of the template.

## Requirements

For convenience, this template is setup to use Pulumi in a specific manner. You will need:

- A GCP bucket
- Access to a Vault instance (with enough permissions to fully use the transit engine for a specific key)
- Ability able to run Devcontainers

## How to use

- Open the latest [gitlab release](https://gitlab.com/geekstuff.dev/templates/go/basic/-/releases)
- Follow the instructions
- Open folder with VSCode

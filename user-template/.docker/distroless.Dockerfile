# syntax=docker/dockerfile:1.0.0-experimental

# Build stage
FROM golang:1.18-alpine as builder

ARG CI_SERVER_HOST
ENV GO111MODULE=on

# Setup workspace
RUN mkdir /workdir && cd /workdir
WORKDIR /workdir

# Setup private repo
ENV GOPROXY=https://proxy.golang.org,direct
ENV GOPRIVATE=${CI_SERVER_HOST}
RUN apk add --no-cache --update openssh \
    && mkdir -p -m 0600 ~/.ssh \
    && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

# Fetch go modules
COPY go.* .
RUN --mount=type=secret,id=auto-devops-build-secrets \
    . /run/secrets/auto-devops-build-secrets \
    && echo "machine ${CI_SERVER_HOST} login ${NETRC_USER} password ${NETRC_PASSWORD}" > ~/.netrc \
    && go mod download \
    && rm -f ~/.netrc

# Build go app
COPY . .
ENV CGO_ENABLED=0
RUN go build -o app -ldflags '-w -s'

# Final stage
FROM gcr.io/distroless/base:debug

COPY --from=builder /workdir/app /app

ENTRYPOINT []
CMD ["/app"]

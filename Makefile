#!make

# NOTE: This Makefile is made with local dev usage in mind

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

CI_PROJECT_URL ?= https://gitlab.com/geekstuff.dev/templates/go/pulumi
CI_REGISTRY_IMAGE ?= tpl/pulumi
CI_REGISTRY_IMAGE_TAG ?= dev
PROJECT_TAG ?= v0.0.0
PULUMI_VERSION ?= 3.33.1

TEMPLATE_DEBIAN_PKG ?= pulumi_devcontainer_${PROJECT_TAG}

PACKAGE_DIR ?= packages

all: info

.PHONY: info
info:
	@echo "Welcome to ${CI_PROJECT_URL}"
	@echo ""
	@echo "Useful commands:"
	@echo "  make devcontainer"
	@echo "  make template"
	@echo "  make template.test"

.PHONY: .docker
.docker:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/${SPECIFIC_IMAGE}:${PROJECT_TAG} \
		-f docker-images/Dockerfile \
		--cache-from "registry.gitlab.com/geekstuff.dev/templates/go/basic/v0.0.2/devcontainer:1.18-bullseye" \
		--cache-from "${CI_REGISTRY_IMAGE}/${SPECIFIC_IMAGE}:${PROJECT_TAG}" \
		--build-arg CI_PROJECT_URL=${CI_PROJECT_URL} \
		--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
		--build-arg PROJECT_TAG=${PROJECT_TAG} \
		--build-arg PULUMI_VERSION=${PULUMI_VERSION} \
		--build-arg SPECIFIC_IMAGE=${SPECIFIC_IMAGE} \
		.
	@docker images ${CI_REGISTRY_IMAGE}/${SPECIFIC_IMAGE}:${PROJECT_TAG}

.PHONY: devcontainer
devcontainer:
	@SPECIFIC_IMAGE=devcontainer $(MAKE) .docker
	docker run --rm ${CI_REGISTRY_IMAGE}/devcontainer:${PROJECT_TAG} \
		cat /devcontainer-go-pulumi-lib/template/.devcontainer/Dockerfile

.PHONY: template
template:
	@mkdir -p ${PACKAGE_DIR}/test-go-pulumi
	@CI_PROJECT_URL=${CI_PROJECT_URL} \
	CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
	CI_REGISTRY_IMAGE_TAG=${CI_REGISTRY_IMAGE_TAG} \
	PROJECT_TAG=${PROJECT_TAG} \
	SPECIFIC_IMAGE=devcontainer \
		scripts/template-bundler.sh \
		user-template/ \
		${PACKAGE_DIR}/${TEMPLATE_DEBIAN_PKG}

.PHONY: template.test
template.test: template.test.cleanup template
	@sh -c "cd ${PACKAGE_DIR}/test-go-pulumi && unzip ../${TEMPLATE_DEBIAN_PKG}.zip" 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-go-pulumi && git init && git add . && git status && git commit -m 'Initial commit'" 1>/dev/null
	@echo "Tests ready at ${PACKAGE_DIR}/test-go-pulumi"

.PHONY: template.test.cleanup
template.test.cleanup:
	@find packages/ -mindepth 1 -delete
